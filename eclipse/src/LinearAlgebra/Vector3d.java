//Maria Barba 1932657
package LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
public static void main(String [] args) {
	Vector3d vector1=new Vector3d(1,1,3);
	double vector1Magnitude=vector1.magnitude();
	System.out.println(vector1Magnitude);//magnitude method test
	
	Vector3d vector2=new Vector3d(1,4,2);
	double dotProductVectors12=vector1.dotProduct(vector2);
	System.out.println(dotProductVectors12);//dotProduct method test
	
	Vector3d vector3=vector1.add(vector2);
	System.out.println(vector3.toString());//add method test
}
	
public Vector3d(double x,double y,double z) {         
		 this.x=x;
		 this.y=y;
		 this.z=z;
}
public double getX() { 
    return this.x; 
}  
public double getY() { 
    return this.y; 
}  
public double getZ() { 
    return this.z; 
} 

public double magnitude () {
	double xPower=Math.pow(this.x, 2);
	double yPower=Math.pow(this.y, 2);
	double zPower=Math.pow(this.z, 2);
	double magnitude=Math.sqrt(xPower+yPower+zPower);
	return magnitude;
}
public double  dotProduct( Vector3d secondVector) {
	double vector2x=secondVector.getX();
	double vector2y=secondVector.getY();
	double vector2z=secondVector.getZ();
	double dotProductResult = (this.x*vector2x)+(this.y*vector2y)+(this.z*vector2z);
	return dotProductResult;
}
public Vector3d add(Vector3d secondVector) {
	double vector2x=secondVector.getX();
	double vector2y=secondVector.getY();
	double vector2z=secondVector.getZ();
	double sumX=this.x+vector2x;
	double sumY=this.y+vector2y;
	double sumZ=this.z+vector2z;
	Vector3d newVector= new Vector3d(sumX,sumY,sumZ);
	return newVector;
}
public String toString() {
	//used to test the add method
	 return ("x : "+ this.x + " y : " + this.y + " z : " + this.z);
}
}
