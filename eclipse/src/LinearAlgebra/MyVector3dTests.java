//Maria Barba 1932657
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MyVector3dTests {

	@Test
	public void testVector3dConstructorGetter() {
		Vector3d myVector=new Vector3d(9,11,5);
		double[] expectedXYZArray= {9,11,5};
         
		   
		double[] outputXYZArray=new double[3];
			outputXYZArray[0]=myVector.getX();
			outputXYZArray[1]=myVector.getY();
			outputXYZArray[2]=myVector.getZ();
		
			assertArrayEquals(expectedXYZArray,outputXYZArray);
	}
	@Test
	public void testVector3dMagnitude() {
		Vector3d vector1=new Vector3d(4,9,2);
		double vector1MagnitudeResult=vector1.magnitude();
		double vector1MagnitudExpected=10.0498;
		assertEquals(vector1MagnitudExpected, vector1MagnitudeResult, 0.0001);
	}
	@Test
	public void testVector3ddotProduct() {
		Vector3d vector1=new Vector3d(3,4,5);
		Vector3d vector2=new Vector3d(6,5,7);
		double resultDotProduct=vector1.dotProduct(vector2);
		double expectedDotProduct=73;
		assertEquals(expectedDotProduct, resultDotProduct);
	}
	@Test
	public void testVector3dAdd() {
		Vector3d vector1=new Vector3d(1,2,3);
		Vector3d vector2=new Vector3d(8,9,10);
		Vector3d vector3=vector1.add(vector2);
		
		double[] expected3Array= {9,11,13};
		
		double[] result3Array=new double[3];
		result3Array[0]=vector3.getX();
		result3Array[1]=vector3.getY();
		result3Array[2]=vector3.getZ();
		assertArrayEquals(expected3Array,result3Array);
	}
	
	

}
